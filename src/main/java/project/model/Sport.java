package project.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import project.model.Location.Locality;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity(name = "Sport")
@Table(name = "sport")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Sport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id_sport;

    @Column(name = "name")
    private String name;

//    @ManyToMany ( cascade = CascadeType.ALL)
//    @JoinTable(name = "practice",
//            joinColumns = @JoinColumn(name = "sport_id"),
//            inverseJoinColumns = @JoinColumn(name = "location_id")
//    )
//
//    @JsonBackReference
//    private List<Locality> localities = new ArrayList<>() ;

    @OneToMany(mappedBy = "sport", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<Practice> practices = new ArrayList<>() ;

    @Override
    public String toString() {
        return "Sport{" +
                "id_sport=" + id_sport +
                ", name='" + name + '\'' +
                ", practices=" + practices +
                '}';
    }

    public void addPractice(Practice p)
    {
        practices.add(p);
    }


    public int getId_sport() {
        return id_sport;
    }

    public void setId_sport(int id_sport) {
        this.id_sport = id_sport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Sport() {
    }


    @Override
    public int hashCode() {
        return Objects.hash(getId_sport(), getName());
    }



}
