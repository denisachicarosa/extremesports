package project.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
public class PracticeId implements Serializable {

    @Column(name = "location_id")
    int location_id;


    @Column(name = "sport_id")
    int sport_id;

    public PracticeId() {
    }

    public PracticeId(int location_id, int sport_id) {
        this.location_id = location_id;
        this.sport_id = sport_id;
    }

    @Override
    public String toString() {
        return "PracticeId{" +
                "location_id=" + location_id +
                ", sport_id=" + sport_id +
                '}';
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }
}