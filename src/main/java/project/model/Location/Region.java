package project.model.Location;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "Region")
@Table(name = "region")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Region  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id_region;

    @Column(name = "name")
    private String name;

    @Column(name = "country_id")
    private int country_id;

    @ManyToOne
    @JoinColumn(name = "country_id", insertable= false, updatable=false)
    @JsonBackReference
    private Country country;

    @OneToMany(mappedBy = "region", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JsonManagedReference
    private List<Locality> localities = new ArrayList<>();


//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Region)) return false;
//        Region region = (Region) o;
//        return getId_region() == region.getId_region() &&
//                Objects.equals(getName(), region.getName());
////                &&
////                Objects.equals(getCountry(), region.getCountry()
////                );
//    }

    @Override
    public int hashCode() {
        return 31;// Objects.hash(getId_region(), getName(), getCountry());
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public List<Locality> getLocalities() {
        return localities;
    }

    public void setLocalities(List<Locality> localities) {
        this.localities = localities;
    }

    @Override
    public String toString() {
        return "Region{" +
                "id_region=" + id_region +
                ", name='" + name + '\'' +
                ", country_id=" + country_id +
                ", country=" + country +
                ", localities=" + localities +
                '}';
    }

//    @Override
//    public String toString() {
//        return "Region{" +
//                "id_region=" + id_region +
//                ", name='" + name + '\'' +
//                ", country=" + country +
//                '}';
//    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getId_region() {
        return id_region;
    }

    public void setId_region(int id_region) {
        this.id_region = id_region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}
