package project.model.Location;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "Country")
@Table(name = "country")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Country implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id_country;

    @Column(name = "name")
    private String name;


    @OneToMany(mappedBy = "country", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JsonManagedReference
    private List<Region> regions = new ArrayList<>();



    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public int getId_country() {
        return id_country;
    }

    public void setId_country(int id_country) {
        this.id_country = id_country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id_country=" + id_country +
                ", name='" + name + '\'' +
                ", regions=" + regions +
                '}';
    }
    public void addRegion(Region region) {
        regions.add(region);
        region.setCountry(this);
    }

    public void removeRegion(Region region) {
        regions.remove(region);
        region.setCountry(null);
    }
}
