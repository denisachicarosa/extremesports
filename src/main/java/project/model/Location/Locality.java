package project.model.Location;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.br.CPF;
import project.model.Practice;
import project.model.Sport;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "Locality")
@Table(name = "locality")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Locality {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id_locality;

    @Column(name = "name")
    private String name;

    @Column(name ="region_id")
    private int region_id;

    @Column(name = "country_id")
    private int country_id;

    @ManyToOne
    @JoinColumn(name = "region_id", insertable= false, updatable=false)
    @JsonBackReference
    private Region region;

    @ManyToOne
    @JoinColumn(name = "country_id", insertable= false, updatable=false)
    @JsonBackReference
    private Country country;



//    @ManyToMany()
//    @JoinTable(name = "practice",
//            joinColumns = @JoinColumn(name = "location_id"),
//            inverseJoinColumns = @JoinColumn(name = "sport_id")
//    )
//    @JsonManagedReference
//    private List<Sport> sports = new ArrayList<>();

    @OneToMany(mappedBy = "location", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private List<Practice> practices = new ArrayList<>() ;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Locality)) return false;
        Locality locality = (Locality) o;
        return getId_locality() == locality.getId_locality() &&
                getRegion_id() == locality.getRegion_id() &&
                getCountry_id() == locality.getCountry_id() &&
                Objects.equals(getName(), locality.getName()) &&
                Objects.equals(getRegion(), locality.getRegion()) &&
                Objects.equals(getCountry(), locality.getCountry());
    }

    @Override
    public String toString() {
        return "Locality{" +
                "id_locality=" + id_locality +
                ", name='" + name + '\'' +
                ", region_id=" + region_id +
                ", country_id=" + country_id +
                ", region=" + region +
                ", country=" + country +
                ", practices=" + practices +
                '}';
    }

    public void addPractice(Practice p)
    {
        practices.add(p);
    }

    public List<Practice> getPractices() {
        return practices;
    }

    public void setPractices(List<Practice> practices) {
        this.practices = practices;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getId_locality() {
        return id_locality;
    }

    public void setId_locality(int id_locality) {
        this.id_locality = id_locality;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }



    @Override
    public int hashCode() {
        return 32;// Objects.hash(getId_locality(), getName(), getRegion());
    }



    public Locality() {
    }

}
