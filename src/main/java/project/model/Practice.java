package project.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import project.controller.SportController;
import project.model.Location.Locality;
import project.model.PracticeId;
import project.service.LocalityService;
import project.service.SportService;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity(name = "Practice")
@Table(name = "practice")
public class Practice implements Serializable {

    @EmbeddedId
    PracticeId id;

    @ManyToOne
    @MapsId("sport_id")
    @JoinColumn(name = "sport_id")
    @JsonBackReference
    private Sport sport;

    @ManyToOne
    @MapsId("location_id")
    @JoinColumn(name = "location_id")
    @JsonBackReference
    private Locality location;

    @Column(name = "price")
    private double price;

    @Column(name = "begin_month")
    private int begin_month;


    @Column(name = "end_month")
    private int end_month;

    public Practice(Integer sport_id, Integer location_id, Double price, Integer begin_month, Integer end_month)
    {
        System.out.println("Constructor" );
        System.out.println(sport_id );
        System.out.println(location_id);
        System.out.println(price);
        System.out.println(begin_month );
        System.out.println(end_month);
        PracticeId pid = new PracticeId( location_id,sport_id);
        SportService ss = new SportService();

        Sport sport_local = new Sport();
        Locality locality = new Locality();
        locality.setId_locality(location_id);
        sport_local.setId_sport(sport_id);

        LocalityService ls = new LocalityService();

        this.setId(pid);
        this.setSport(sport_local);
        this.setLocation(locality);
        this.setPrice(price);
        this.setBegin_month(begin_month);
        this.setEnd_month(end_month);
    }

    public Practice() {
    }

    public Practice(PracticeId id, Sport sport, Locality location, double price, int begin_month, int end_month) {
        this.id = id;
        this.sport = sport;
        this.location = location;
        this.price = price;
        this.begin_month = begin_month;
        this.end_month = end_month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Practice)) return false;
        Practice practice = (Practice) o;
        return Double.compare(practice.getPrice(), getPrice()) == 0 &&
                Objects.equals(getId(), practice.getId()) &&
                Objects.equals(getSport(), practice.getSport()) &&
                Objects.equals(getLocation(), practice.getLocation()) &&
                Objects.equals(getBegin_month(), practice.getBegin_month()) &&
                Objects.equals(getEnd_month(), practice.getEnd_month());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSport(), getLocation(), getPrice(), getBegin_month(), getEnd_month());
    }

    @Override
    public String toString() {
        return "Practice{" +
                "id=" + id +
                ", sport=" + sport +
                ", location=" + location +
                ", price=" + price +
                ", begin_month='" + begin_month + '\'' +
                ", end_month='" + end_month + '\'' +
                '}';
    }

    public PracticeId getId() {
        return id;
    }

    public void setId(PracticeId id) {
        this.id = id;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Locality getLocation() {
        return location;
    }

    public void setLocation(Locality location) {
        this.location = location;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getBegin_month() {
        return begin_month;
    }

    public void setBegin_month(int begin_month) {
        this.begin_month = begin_month;
    }

    public int getEnd_month() {
        return end_month;
    }

    public void setEnd_month(int end_month) {
        this.end_month = end_month;
    }
}


