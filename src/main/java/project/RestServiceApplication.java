package project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServiceApplication {

	public static void main(String[] args)
	{
		System.setProperty("tomcat.util.http.parser.HttpParser.requestTargetAllow", "{}[]");

		SpringApplication.run(RestServiceApplication.class, args);

	}

}
