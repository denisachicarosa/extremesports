package project.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dao.SportDao;
import project.model.Sport;
import project.repository.SportRepo;
import project.service.interfaces.SportServiceInterface;

import java.util.List;

@Service
public class SportService implements SportServiceInterface {
    @Autowired
    SportRepo sportRepository;

    @Autowired
    SportDao sportDao;

    @Override
    public Sport getSport(Integer id) {
        System.out.print("get sport with id =");
        System.out.println(id);
        return sportRepository.getOne(id);
    }

    @Override
    public List<Sport> getAll() {
        return sportRepository.findAll();
    }

    @Override
    public Sport addSport(Sport sport) {
        return sportRepository.saveAndFlush(sport);
    }

    @Override
    public Sport updateSport(Integer id, Sport sport) {
        Sport existingSport = sportRepository.getOne(id);
        BeanUtils.copyProperties(sport, existingSport);
        return sportRepository.saveAndFlush(existingSport);
    }

    @Override
    public Sport deleteSport(Integer id) {
        Sport existingSport = sportRepository.getOne(id);
        sportRepository.delete(existingSport);
        return existingSport;
    }

    @Override
    public List<String> findSport(String sport_list, String begin, String end)
    {
        List<String> result;

        result = sportDao.getSport(sport_list, getMonth(begin), getMonth(end));

        return result;
    }

    public enum Months
    {
        JANUARY(1), FEBRUARY(2), MARCH(3), APRIL(4), MAY(5), JUNE(6), JULY(7), AUGUST(8), SEPTEMBER(9), OCTOBER(10), NOVEMBER(11), DECEMBER(12);

        int monthOrdinal = 0;

        Months(int ord)
        {
            this.monthOrdinal = ord;
        }

        public static Months[] MONTHS_INDEXED = new Months[] { null, JANUARY, FEBRUARY, MARCH, APRIL, MAY, JUNE, JULY, AUGUST, SEPTEMBER, OCTOBER, NOVEMBER, DECEMBER };

        public static int getIndex(String m)
        {
          for (Months mon : Months.values())
          {

              System.out.println(mon.monthOrdinal);
              if (mon.name().equals(m.toUpperCase()))
                  return mon.monthOrdinal;
          }
          return 0;
        }
    }

    public int getMonth(String date)
    {
        System.out.println(date);
        int index = date.indexOf(' ');
        date = date.substring(index + 1,date.length());
        return Months.getIndex(date);
    }

}
