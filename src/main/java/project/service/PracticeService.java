package project.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dao.PracticeDao;
import project.model.Location.Locality;
import project.model.Practice;
import project.model.Sport;
import project.repository.PracticeRepo;
import project.service.interfaces.PracticeServiceInterface;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class PracticeService implements PracticeServiceInterface {
    @Autowired
    PracticeRepo practiceRepository;

    @Autowired
    PracticeDao practiceDao;

    @Override
    public List<Practice> getPractice(Integer id_sport, Integer id_location) {
        return practiceDao.getById(id_sport, id_location);
    }

    @Override
    public List<Practice> getAll() {
        return practiceRepository.findAll();
    }

    @Override
    public Practice addPractice(Practice practice) {
        System.out.println("Practice service : addPractice");
        System.out.println(practice.toString());
        return practiceRepository.saveAndFlush(practice);
    }

    @Override
    public Practice updatePractice(Integer sport_id, Integer location_id, Practice practice) {
        List<Practice> existing = practiceDao.getById(sport_id, location_id);
        Practice existingPractice = existing.get(0);
        System.out.println("Existing : " + existingPractice.toString());
        Sport s = new Sport();
        s.setId_sport(existingPractice.getId().getSport_id());

        Locality l = new Locality();
        l.setId_locality(existingPractice.getId().getLocation_id());

        System.out.println("New updated : " + practice.toString());

        BeanUtils.copyProperties(practice, existingPractice);
        return practiceRepository.saveAndFlush(existingPractice);
    }

    @Override
    public Practice deletePractice(Integer sport_id, Integer location_id) {
        List<Practice> lp =practiceDao.getById(sport_id, location_id);
        Practice existingPractice = lp.get(0);
        practiceRepository.delete(existingPractice);
        return existingPractice;
    }
}
