package project.service.interfaces;

import org.springframework.stereotype.Component;
import project.model.Practice;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Component
public interface PracticeServiceInterface  {
    List<Practice> getPractice(Integer id_sport, Integer id_location);
    List<Practice> getAll();
    Practice addPractice(Practice practice);
    Practice updatePractice(Integer sport_id, Integer location_id, Practice practice);
    Practice deletePractice(Integer sport_id, Integer location_id);

}
