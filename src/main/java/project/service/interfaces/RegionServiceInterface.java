package project.service.interfaces;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import project.model.Location.Region;

import java.util.List;

@Component
public interface RegionServiceInterface {
    Region getRegion(Integer id);
    List<Region> getAll();
    Region addRegion(Region region);
    Region updateRegion(Integer id, Region region);
    Region deleteRegion(Integer id);
}
