package project.service.interfaces;

import org.springframework.stereotype.Component;
import project.model.Sport;

import java.util.List;

@Component
public interface SportServiceInterface {
    Sport getSport(Integer id);
    List<Sport> getAll();
    Sport addSport(Sport sport);
    Sport updateSport(Integer id, Sport sport);
    Sport deleteSport(Integer id);
    List<String> findSport(String sport_list, String begin, String end);

}
