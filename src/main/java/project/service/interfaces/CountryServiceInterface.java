package project.service.interfaces;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import project.model.Location.Country;

import java.util.List;

@Component
public interface CountryServiceInterface {
    Country getCountry(Integer id);
    List<Country> getAll();
    Country addCountry(Country country);
    Country updateCountry(Integer id, Country country);
    Country deleteCountry(Integer id);
}
