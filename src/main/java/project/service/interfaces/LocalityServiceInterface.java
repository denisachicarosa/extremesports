package project.service.interfaces;

import org.springframework.stereotype.Component;
import project.model.Location.Locality;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
public interface LocalityServiceInterface {
     Locality getLocality(Integer id);
     List<Locality> getAll();
     Locality addLocality(Locality locality);
     Locality updateLocality(Integer id, Locality locality);
     Locality deleteLocality(Integer id);
//    public Pair<Locality, List<Role>> getRolesForLocality(Integer id);
}
