package project.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.model.Location.Region;
import project.repository.RegionRepo;
import project.service.interfaces.RegionServiceInterface;

import java.util.List;

@Service
public class RegionService implements RegionServiceInterface {
    @Autowired
    RegionRepo regionRepository;

    @Override
    public Region getRegion(Integer id) {
        return regionRepository.getOne(id);
    }

    @Override
    public List<Region> getAll() {
        return regionRepository.findAll();
    }

    @Override
    public Region addRegion(Region region) {
        return regionRepository.saveAndFlush(region);
    }

    @Override
    public Region updateRegion(Integer id, Region region) {
        Region existingRegion = regionRepository.getOne(id);
        BeanUtils.copyProperties(region, existingRegion);
        return regionRepository.saveAndFlush(existingRegion);
    }

    @Override
    public Region deleteRegion(Integer id) {
        System.out.print("\n In Service : Method delete region with parameters: ");
        System.out.print(id);
        Region existingRegion = regionRepository.getOne(id);
        regionRepository.delete(existingRegion);
        return existingRegion;
    }

}
