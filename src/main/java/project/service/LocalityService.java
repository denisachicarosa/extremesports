package project.service;

import project.repository.LocalityRepo;
import project.model.Location.Locality;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.service.interfaces.LocalityServiceInterface;

import java.util.List;

@Service
public class LocalityService implements LocalityServiceInterface {
    @Autowired
    LocalityRepo localityRepository;

    @Override
    public Locality getLocality(Integer id) {
        return localityRepository.getOne(id);
    }

    @Override
    public List<Locality> getAll() {
        return localityRepository.findAll();
    }

    @Override
    public Locality addLocality(Locality locality) {
        return localityRepository.saveAndFlush(locality);
    }

    @Override
    public Locality updateLocality(Integer id, Locality locality) {
        Locality existingLocality = localityRepository.getOne(id);
        BeanUtils.copyProperties(locality, existingLocality);
        return localityRepository.saveAndFlush(existingLocality);
    }

    @Override
    public Locality deleteLocality(Integer id) {
        Locality existingLocality = localityRepository.getOne(id);
        localityRepository.delete(existingLocality);
        return existingLocality;
    }

//    @Override
//    public Pair<Locality, List<Role>>  getRolesForLocality(Integer id) {
//        log.debug("get roles for locality in LocalityService");
//        Locality locality = getLocality(id);
//        List<Role> roles = localityDao.getRolesForLocality(id);
//        log.debug(" received list of roles ");
//
//        Pair<Locality, List<Role>> pair = Pair.of(locality, roles);
//        return pair;
//    }
}
