package project.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.model.Location.Country;
import project.repository.CountryRepo;
import project.service.interfaces.CountryServiceInterface;


import java.util.List;

@Service
public class CountryService implements CountryServiceInterface {

    @Autowired
    CountryRepo countryRepository;

    @Override
    public Country getCountry(Integer id) {
        System.out.println("RequestMethod.GET, go to countryRepo.getOne(id)");
        return countryRepository.getOne(id);
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country addCountry(Country country) {
        return countryRepository.saveAndFlush(country);
    }

    @Override
    public Country updateCountry(Integer id, Country country) {
        Country existingCountry = countryRepository.getOne(id);
        BeanUtils.copyProperties(country, existingCountry);
        return countryRepository.saveAndFlush(existingCountry);
    }

    @Override
    public Country deleteCountry(Integer id) {
        Country existingCountry = countryRepository.getOne(id);
        countryRepository.delete(existingCountry);
        return existingCountry;
    }
}
