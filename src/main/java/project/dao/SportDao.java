package project.dao;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class SportDao {

    private JdbcTemplate template;
    @Autowired
    public void setJdbcTemplate(JdbcTemplate template) {this.template = template;}

    //prepare string for query. Needs to have the following format ("sport1", "sport2", ...)
    public String splitString(String list)
    {
        String result = "";
        result += "(\"";
        result += list.replace("|","\",\"");
        result += "\")";
        return result;

    }

    public List<String> getSport(String sport_list, int begin, int end)
    {
        System.out.println("begin : " );

        System.out.println(begin );
        return template.query(("SELECT c.name,\n" +
                "\tr.name,\n" +
                "    l.name,\n" +
                "    s.name,\n" +
                "    p.price\n" +
                "FROM country c\n" +
                "JOIN region r on r.country_id = c.id\n" +
                "JOIN locality l on l.region_id = r.id\n" +
                "JOIN practice p on p.location_id = l.id\n" +
                "JOIN sport s on p.sport_id = s.id\n" +
                "WHERE s.name in " + splitString(sport_list) +"\n"+
                "AND begin_month <= ? \n" +
                "AND end_month >= ? \n" +
                "order by p.price "),
                new Object[]{begin, end},
                new RowMapper<String>(){
                @Override
                    public String mapRow(ResultSet resultSet, int i) throws SQLException {
                    String result = "";
                    result += resultSet.getString((1));
                    result += ", ";
                    result += resultSet.getString((2));
                    result += ", ";
                    result += resultSet.getString((3));
                    result += " : ";
                    result += resultSet.getString((4));
                    result += " price :  ";
                    result += resultSet.getString((5));
                    return result;
                }
                });

    }
}
