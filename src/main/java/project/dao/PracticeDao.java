package project.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import project.model.Practice;
import project.model.PracticeId;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class PracticeDao {
    private JdbcTemplate template;
    @Autowired
    public void setJdbcTemplate(JdbcTemplate template) {this.template = template;}

    public List<Practice> getById(Integer id_sport, Integer id_location) {

        return template.query
                ("SELECT sport_id,\n" +
                    "\tlocation_id,\n" +
                    "    price,\n" +
                    "    begin_month,\n" +
                    "    end_month\n" +
                    " FROM practice\n" +
                    " WHERE sport_id = ?\n" +
                    "\tAND location_id = ? ",
                        new Object[]{id_sport, id_location}, new RowMapper<Practice>() {
                            @Override
                            public  Practice mapRow(ResultSet resultSet, int i) throws SQLException {
                                Practice p = new Practice();
                                PracticeId pid = new PracticeId();
                                pid.setSport_id(resultSet.getInt(1));
                                pid.setLocation_id(resultSet.getInt(2));
                                p.setId(pid);
                                p.setPrice(resultSet.getDouble(3));
                                p.setBegin_month(resultSet.getInt(4));
                                p.setEnd_month(resultSet.getInt(5));
                                return p;
                            }
                        });
    }
}
