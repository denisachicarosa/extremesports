package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.Location.Region;

@Repository
public interface RegionRepo  extends JpaRepository<Region, Integer> {
}
