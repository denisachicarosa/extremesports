package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.Location.Country;

@Repository
public interface CountryRepo extends JpaRepository<Country, Integer> {
}
