package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.Sport;

@Repository
public interface SportRepo extends JpaRepository<Sport, Integer> {
}
