package project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import project.model.Practice;
import project.model.Sport;

@Repository

public interface PracticeRepo extends JpaRepository<Practice, Integer> {
}
