package project.controller;

import org.springframework.http.MediaType;
import project.model.Location.Locality;
import project.service.LocalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admin")
public class LocalityController {

    @Autowired
    LocalityService localityService;

    @GetMapping(value = "localities")
    public List<Locality> list() {
        System.out.println("RequestMethod.GET, go to localityRepository.findAll()");
        return localityService.getAll();
    }

    @PostMapping(value = "localities/create")
    public Locality create( Locality locality) {
        System.out.println("create Note - > Post Mapping");
        return localityService.addLocality(locality);
    }

    @RequestMapping(value = "localities/{id}", method = RequestMethod.GET)
    public Locality get(@PathVariable("id") Integer id) {
        return localityService.getLocality(id);
    }


    @PutMapping(value = "localities/update/{id}", consumes= MediaType.APPLICATION_JSON_VALUE)
    public Locality update(@PathVariable Integer id, Locality locality) {
        return localityService.updateLocality(id, locality);
    }

    @DeleteMapping(value = "localities/delete/{id}")
    public Locality delete(@PathVariable("id") Integer id) {
        return localityService.deleteLocality(id);
    }

//    @RequestMapping(value = "locality_and_roles/{id}", method = RequestMethod.GET)
//    public Pair<Locality, List<Role>> gerLocalityRoles(@PathVariable("id") Integer id) {
//        log.debug(" in controlles, method getLocalityRoles with id = " + id);
//        return localityService.getRolesForLocality(id);
//    }

}
