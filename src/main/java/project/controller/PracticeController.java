package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import project.model.Location.Locality;
import project.model.Practice;
import project.model.Sport;
import project.service.LocalityService;
import project.service.PracticeService;
import project.service.SportService;

import java.util.List;

@RestController
@RequestMapping("admin")
public class PracticeController {
    @Autowired
    PracticeService practiceService;

    @GetMapping(value = "practice")
    public List<Practice> list() {
        System.out.println("RequestMethod.GET, go to practiceRepository.findAll()");
        return practiceService.getAll();
    }

    @PostMapping(value = "practice/create/{sport_id}/{location_id}/",produces = MediaType.APPLICATION_JSON_VALUE)
    public Practice create(@PathVariable("sport_id")Integer id_s, @PathVariable("location_id") Integer id_location, Practice practice) {
        Practice p = new Practice(id_s,id_location,practice.getPrice(), practice.getBegin_month(), practice.getEnd_month());
        System.out.println("create Note - > Post Mapping");
        return practiceService.addPractice(p);
    }

    @GetMapping(value = "practice/{id_sport}/{id_location}")
    public List<Practice> get(@PathVariable("id_sport") Integer id_sport, @PathVariable("id_location") Integer id_location) {
        System.out.println("RequestMethod.GET, go to practiceService.getPractice(id)");
        return practiceService.getPractice(id_sport, id_location);
    }


    @PutMapping(value = "practice/update/{sport_id}/{location_id}",  consumes=MediaType.APPLICATION_JSON_VALUE)
    public Practice update(@PathVariable("sport_id") Integer sport_id,@PathVariable("location_id") Integer location_id, Practice practice) {
        System.out.print("Method put practice with parameters: ");
        System.out.println(sport_id);
        System.out.println(location_id);
        Practice p = new Practice(sport_id, location_id, practice.getPrice(), practice.getBegin_month(), practice.getEnd_month());

        return practiceService.updatePractice(sport_id, location_id, p);
    }

    @DeleteMapping(value = "practice/delete/{sport_id}/{location_id}")
    public Practice delete(@PathVariable("sport_id") Integer sport_id, @PathVariable("location_id") Integer location_id)
    {
        System.out.print("Method delete practice with parameters: ");
        System.out.print(sport_id);System.out.print(" "); System.out.println(location_id);
        return practiceService.deletePractice(sport_id, location_id);
    }

}
