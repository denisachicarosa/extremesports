package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import project.model.Location.Region;
import project.service.RegionService;


import java.util.List;

@RestController
@RequestMapping("admin")
public class RegionController {
    @Autowired
    RegionService regionService;

    @GetMapping(value = "regions")
    public List<Region> list() {
        System.out.println("RequestMethod.GET, go to regionRepository.findAll()");
        return regionService.getAll();
    }

    @PostMapping(value = "regions/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public Region create(Region region) {
        System.out.println("create Note - > Post Mapping");
        return regionService.addRegion(region);
    }

    @RequestMapping(value = "regions/{id}", method = RequestMethod.GET)
    public Region get(@PathVariable("id") Integer id) {
        return regionService.getRegion(id);
    }


    @PutMapping(value = "regions/update/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
    public Region update(@PathVariable Integer id, Region region) {
        return regionService.updateRegion(id, region);
    }

    @DeleteMapping(value = "regions/delete/{id}")
    public Region delete(@PathVariable("id") Integer id) {
        System.out.print("Method delete region with parameters: ");
        System.out.print(id);
        return regionService.deleteRegion(id);
    }
}
