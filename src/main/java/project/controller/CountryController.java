package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import project.model.Location.Country;
import project.service.CountryService;


import java.util.List;

@RestController
@RequestMapping("admin")
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping(value = "countries")
    public List<Country> list() {
        System.out.println("RequestMethod.GET, go to countryRepository.findAll()");
        return countryService.getAll();
    }

    @PostMapping(value = "countries/create",produces = MediaType.APPLICATION_JSON_VALUE)
    public Country create( Country country) {
        System.out.println("create Note - > Post Mapping");
        return countryService.addCountry(country);
    }

    @GetMapping(value = "countries/{id}")
    public Country get(@PathVariable("id") Integer id) {
        System.out.println("RequestMethod.GET, go to countryService.getCountry(id)");

        return countryService.getCountry(id);
    }


    @PutMapping(value = "countries/update/{id}",  consumes=MediaType.APPLICATION_JSON_VALUE)
    public Country update(@PathVariable("id") Integer id, Country country) {
        System.out.print("Method put country with parameters: ");
        System.out.print(id);
        System.out.print(" and " + country.toString());


        return countryService.updateCountry(id, country);
    }

    @DeleteMapping(value = "countries/delete/{id}")
    public Country delete(@PathVariable("id") Integer id)
    {
        System.out.print("Method delete country with parameters: ");
        System.out.print(id);
        return countryService.deleteCountry(id);
    }

}
