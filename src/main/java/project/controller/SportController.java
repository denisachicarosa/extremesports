package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import project.model.Sport;
import project.service.SportService;


import java.util.List;

@RestController
@RequestMapping()
public class SportController {
    @Autowired
    SportService sportService;

    @GetMapping(value = "admin/sports")
    public List<Sport> list() {
        System.out.println("RequestMethod.GET, go to sportRepository.findAll()");
        return sportService.getAll();
    }

    @PostMapping(value = "admin/sports/create",produces = MediaType.APPLICATION_JSON_VALUE)
    public Sport create( Sport sport) {
        System.out.println("create Note - > Post Mapping");
        return sportService.addSport(sport);
    }

    @GetMapping(value = "admin/sports/{id}")
    public Sport get(@PathVariable("id") Integer id) {
        System.out.println("RequestMethod.GET, go to sportService.getSport(id)");

        return sportService.getSport(id);
    }


    @PutMapping(value = "admin/sports/update/{id}",  consumes=MediaType.APPLICATION_JSON_VALUE)
    public Sport update(@PathVariable("id") Integer id, Sport sport) {
        System.out.print("Method put sport with parameters: ");
        System.out.print(id);
        System.out.print(" and " + sport.toString());


        return sportService.updateSport(id, sport);
    }

    @DeleteMapping(value = "admin/sports/delete/{id}")
    public Sport delete(@PathVariable("id") Integer id)
    {
        System.out.print("Method delete sport with parameters: ");
        System.out.print(id);
        return sportService.deleteSport(id);
    }

    @GetMapping(value = "user/search/{sport_list}/{begin}/{end}")
    public List<String> findSports(@PathVariable("sport_list") String sport_list, @PathVariable("begin") String begin, @PathVariable("end") String end)
    {
        System.out.println("Begin date: " +begin);
        System.out.println("End date: " +end);

        return sportService.findSport(sport_list, begin, end);
    }

}
