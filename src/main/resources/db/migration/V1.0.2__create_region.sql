
CREATE TABLE IF NOT EXISTS extreme_sports.region (
  id INT NOT NULL AUTO_INCREMENT,
  country_id INT NOT NULL,
  name VARCHAR(100),
  PRIMARY KEY (id)
);

ALTER TABLE extreme_sports.region ADD CONSTRAINT Fk_region FOREIGN KEY (country_id) REFERENCES country(id) ON DELETE CASCADE;
