CREATE SCHEMA IF NOT EXISTS extreme_sports;

CREATE TABLE IF NOT EXISTS extreme_sports.hibernate_sequence (
  next_val bigint not null AUTO_INCREMENT,
PRIMARY KEY(next_val)
);


CREATE TABLE IF NOT EXISTS extreme_sports.user (
  id_user INT NOT NULL AUTO_INCREMENT,
  id_role INT NOT NULL,
  username VARCHAR(100),
  password VARCHAR(100),
PRIMARY KEY (id_user)
);


CREATE TABLE IF NOT EXISTS extreme_sports.role (
  id_role INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100),
PRIMARY KEY (id_role)
);




ALTER TABLE extreme_sports.user ADD CONSTRAINT Fk1_user FOREIGN KEY (id_role) REFERENCES role(id_role)  ON DELETE CASCADE;
