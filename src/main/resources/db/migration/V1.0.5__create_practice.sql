
CREATE TABLE IF NOT EXISTS extreme_sports.practice (
  sport_id int not null,
  location_id int not null,
  price DOUBLE,
  begin_month INT,
  end_month INT,
  PRIMARY KEY(sport_id, location_id)
);



ALTER TABLE extreme_sports.practice ADD CONSTRAINT Fk1_practice FOREIGN KEY (sport_id) REFERENCES sport(id) ON DELETE CASCADE;
ALTER TABLE extreme_sports.practice ADD CONSTRAINT Fk2_practice FOREIGN KEY (location_id) REFERENCES locality(id) ON DELETE CASCADE;
