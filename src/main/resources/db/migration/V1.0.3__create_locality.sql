
CREATE TABLE IF NOT EXISTS extreme_sports.locality (
  id int not null AUTO_INCREMENT,
  region_id int not null,
  country_id int not null,
  name VARCHAR(20),
  PRIMARY KEY (id)
);



ALTER TABLE extreme_sports.locality ADD CONSTRAINT Fk1_locality FOREIGN KEY (region_id) REFERENCES region(id) ON DELETE CASCADE;
ALTER TABLE extreme_sports.locality ADD CONSTRAINT Fk2_locality FOREIGN KEY (country_id) REFERENCES country(id) ON DELETE CASCADE;
