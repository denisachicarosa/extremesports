Java Spring project -> manage extreme sports in location hierarchy: Country -> Region -> Locality

I used Postman while testing services.

Examples of end-points to test services:

Countries:

* [x] POST: localhost:8080/admin/countries/create?name="Germany"
* [x] GET: localhost:8080/admin/countries
* [x] GET: localhost:8080/admin/countries/2
* [x] PUT:localhost:8080/admin/countries/update/5?id_country=5&name=India
* [x] DELETE: localhost:8080/admin/countries/delete/6


Regions:

* [x] POST: localhost:8080/admin/regions/create?name=Grand East&country_id=2
* [x] GET: localhost:8080/admin/regions
* [x] GET: localhost:8080/admin/regions/3
* [x] PUT:localhost:8080/admin/regions/update/6?id_region=6&country_id=4&name=Sofia
* [x] DELETE: localhost:8080/admin/regions/delete/1


Localities:

* [x] POST: localhost:8080/admin/localities/create?name=Provence Alpest&country_id=2&region_id=3
* [x] GET: localhost:8080/admin/localities
* [x] GET: localhost:8080/admin/localities/3
* [x] PUT:localhost:8080/admin/localities/update/7?id_locality=7&region_id=3&country_id=2&name=Provence Alpes
* [x] DELETE: localhost:8080/admin/localities/delete/1



Sports:

* [x] POST: localhost:8080/admin/sports/create?name=Cliff climbing
* [x] GET: localhost:8080/admin/sports/
* [x] GET: localhost:8080/admin/sports/1
* [x] PUT: localhost:8080/admin/sports/update/2?id_sport=2&name=Hiking
* [x] DELETE: localhost:8080/admin/sports/delete/6

Practices:
* [x] POST: localhost:8080/admin/practice/create/2/2/?price=20&begin_month=1&end_month=4
* [x] GET: localhost:8080/admin/practice
* [x] GET: localhost:8080/admin/practice/1/2
* [x] PUT: localhost:8080/admin/practice/update/2/2/?sport_id=2&location_id=2&price=35&begin_month=1&end_month=4
* [x] DELETE:localhost:8080/admin/practice/delete/2/2


Filter motor for regular users:
* [x] GET: localhost:8080/user/search/swiming|hiking/10 June/8 August

Mention: sports have to be separated with | and date is in format: dd month